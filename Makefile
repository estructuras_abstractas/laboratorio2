ALL: comp doxygen

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 2s

	sudo apt install doxygen
	sudo apt install doxygen-gui
	sudo apt-get install graphviz
	sudo apt install texlive-latex-extra
	sudo apt install texlive-lang-spanish

comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 2s

	mkdir -p bin build docs

	g++ -o ./build/in_out.o -c ./src/in_out.cpp
	g++ -o ./build/compresor.o -c ./src/compresor.cpp
	g++ -o ./build/main.o -c ./src/main.cpp
	
	g++ -o ./bin/lab2 ./build/main.o ./build/compresor.o ./build/in_out.o

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 2s
	
	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

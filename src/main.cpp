#include <iostream>
#include <time.h> //< Incluye las funciones clock_t, clock(), CLOCKS_PER_SEC y cpu_time_used

#include "../include/compresor.h"

#define OK 0

using namespace std;

int main( int argc, char** argv ){

    /** Inicia la cuenta de tiempo de ejecucion del programa */
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    Compresor compresor( argc, argv );
    
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    cout << "Tiempo invertido = " << cpu_time_used << " segundos";
    cout << endl;
    
    return OK;
};
#include "../include/compresor.h"

void Compresor::print_usage()
{
    cout << "--------------------------------";
    cout << endl;
    cout << endl;    
    cout << " Debe escribir en la terminal:" << endl << endl;
    cout << " ----> Para comprimir:    ./bin/lab2 comprimir 'numero_umbral' 'nombre.txt'";
    cout <<  endl << endl;
    cout << " ----> Para descomprimir: ./bin/lab2 descomprimir 'nombre.tab' 'nombre.rep'";
    cout << endl;
    cout << endl;
    cout << "--------------------------------";
    cout << endl;
};


void Compresor::print_error()
{
    cout << "--------------------------------";
    cout << endl;
    cout << endl; 
    cout << "Error inesperado, contacte con Jorge Munoz Taylor con el correo";
    cout << " jorge.munoztaylor@ucr.ac.cr para verificar el codigo.";
    cout << endl;
    cout << endl; 
    cout << "--------------------------------";
    cout << endl;
;}


void Compresor::imprimir_error( int retorno)
{
    switch(retorno)
    {
        case EXITO: 
            break;
        
        case FALLO: 
            this->print_usage();
            break;
        
        case ERROR:
            this->print_error();
            break;

        case ARCHIVO_NO_EXISTE:
            cout << "--------------------------------";
            cout << endl;
            cout << endl; 
            cout << "El archivo no existe o la direccion no es correcta :V";
            cout << endl;
            cout << endl; 
            cout << "--------------------------------";
            cout << endl; 

            this->print_usage();  
            break;

        default:
            this->print_error();
            this->print_usage();
            break;
    }
};


Compresor::Compresor(int argc, char** argv )
{
    /**
     * Declaracion de parametros y objetos.
     */
    int    _retorno = FALLO;
    string _cadena_base;
    string _cadena_comparar;
    string _palabra_base;
    string _palabra_comparar;
    string _direccion_rep;
    string _direccion_tab;
    int    _indice_base = 0;
    int    _indice_comparar = 0;
    int    _contador_palabras;
    bool   _palabra_repetida;

    entrada_salida_texto Entrada_Salida;
        
    /**
     * Verifica si se ingresaron exactamente dos argumentos. 
     */
    _retorno = Entrada_Salida.Verifica_Parametros_Entrada( argc, argv );
    
    if( _retorno == EXITO )
    {
        if( string( argv[1] ) == "comprimir" )
        {
            Entrada_Salida.Sacar_Raiz_Direccion( argv, _direccion_rep, _direccion_tab );

            /**
             * Se crean y se abren todos los archivos necesarios para trabajar.
             */
            ifstream archivo_base( argv[3] );
            if( !archivo_base ) {cout<<endl<<"No se abrió el archivo suministrado :("; return;}
            
            ofstream in_tab;
            in_tab.open ( _direccion_tab.c_str() );
            if( !in_tab ) {cout<<endl<<"No se abrió el archivo .tab :("; return;}
            else cout << "--> Archivo creado: " <<_direccion_tab << endl;

            ofstream in_rep;
            in_rep.open( _direccion_rep.c_str() );
            if( !in_rep ) {cout<<endl<<"No se abrió el archivo .rep"; return;}
            else cout << "--> Archivo creado: " << _direccion_rep << endl;


            ifstream in_tab_temp( _direccion_tab.c_str() );
            if( !in_tab_temp ) {cout<<endl<<"No se abrió el archivo in_tab_temp :("; return;}
            

            ofstream atemp( "./temporal.txt" );
            if( !atemp ) {cout<<endl<<"No se abrió el archivo temporal.txt :("; return;}

            ifstream archivo_temporal( "./temporal.txt");
            if( !archivo_temporal ) {cout<<endl<<"No se abrió el archivo temporal.txt :("; return;}

            ifstream archivo_temporal_2( "./temporal.txt");
            if( !archivo_temporal_2 ) {cout<<endl<<"No se abrió el archivo temporal.txt :("; return;}


            /**
            * Extrae todas las palabras en un archivo de txt.
            */
            while( !archivo_base.eof() )
            {
                _indice_base = 0;
                _cadena_base.clear();
        
                getline( archivo_base, _cadena_base ); //Lee una linea
                                    
                for (int i = 0; i<Entrada_Salida.Contar_Palabras( _cadena_base ); i++)
                {
                    Entrada_Salida.Sacar_Palabra ( _cadena_base, _indice_base, _palabra_base );                 
            
                    /* Escribe la palabra en el archivo temporal.txt */
                    atemp << _palabra_base;
                    atemp << endl;
                } 
                    
            }

            /**
             * Compara las palabras del texto con las palabras del archivo .tab
             * y suma en la variable _contador_palabras si son iguales.
             */
            archivo_temporal.clear();
            archivo_temporal.seekg( in_tab.beg );
            while( !archivo_temporal.eof() )
            {
                _contador_palabras = 0;
                _palabra_base.clear();
                getline( archivo_temporal, _palabra_base );
                _palabra_repetida = false;

                in_tab_temp.clear();
                in_tab_temp.seekg( in_tab_temp.beg );  
                while( !in_tab_temp.eof() )
                {
                    _indice_comparar = 0;
                    _cadena_comparar.clear();
                    _palabra_comparar.clear();
                    getline( in_tab_temp, _cadena_comparar );   
                    Entrada_Salida.Sacar_Palabra ( _cadena_comparar, _indice_comparar, _palabra_comparar );
                    
                    if( _palabra_base == _palabra_comparar )
                    {    
                        _palabra_repetida = true;          
                        break;
                    }

                }

                if( _palabra_repetida == false )
                {
                    archivo_temporal_2.clear();
                    archivo_temporal_2.seekg( in_tab.beg );
                    while( !archivo_temporal_2.eof() )
                    {
                        getline( archivo_temporal_2, _palabra_comparar );

                        if( _palabra_base == _palabra_comparar )
                        {
                            _contador_palabras++;
                        }

                    } 

                    if( _contador_palabras >= atoi(argv[2]) )
                    {
                        in_tab << _palabra_base << " " << Entrada_Salida.Codigo();
                        in_tab << endl;
                    }
                }
            } 

            /**
             * Se imprimen las palabras en el archivo .rep
             */
            archivo_base.clear();
            archivo_base.seekg( archivo_base.beg );
            while( !archivo_base.eof() )
            {
                _indice_base = 0;
                _cadena_base.clear();
                _palabra_base.clear();
                getline( archivo_base, _cadena_base );

                for (int i = 0; i<Entrada_Salida.Contar_Palabras( _cadena_base ); i++)
                {
                    Entrada_Salida.Sacar_Palabra ( _cadena_base, _indice_base, _palabra_base ); 

                    _palabra_repetida = false;

                    in_tab_temp.clear();
                    in_tab_temp.seekg( in_tab_temp.beg );
                    while( !in_tab_temp.eof() )
                    {
                        _indice_comparar = 0;
                        _cadena_comparar.clear();
                        _palabra_comparar.clear();

                        getline( in_tab_temp, _cadena_comparar );
                        Entrada_Salida.Sacar_Palabra( _cadena_comparar, _indice_comparar, _palabra_comparar);

                        if( _palabra_base == _palabra_comparar )
                        {  
                            _palabra_repetida = true;
                            Entrada_Salida.Sacar_Palabra( _cadena_comparar, _indice_comparar, _palabra_comparar);
                            in_rep << _palabra_comparar << " ";
                        } 

                    }               
                    
                    if( !_palabra_repetida )
                    {
                        in_rep << _palabra_base << " ";
                    }

                } 

                in_rep << endl;

            } 

            archivo_base.close();
            archivo_temporal.close();
            archivo_temporal_2.close();
            in_tab.close();
            in_rep.close();
            in_tab_temp.close();
            atemp.close();

            archivo_base.clear();
            archivo_temporal.clear();
            archivo_temporal_2.clear();
            in_tab.clear();
            in_rep.clear();
            in_tab_temp.clear();
            atemp.clear();

            remove( "temporal.txt" );

        } 

        /**
         * Descomprime el contenido del .rep en un archivo _descomprimido.txt,
         * basicamente se sigue el procedimiento inverso al caso de compresion, solo
         * que en este caso se utilizo el tipo map para trabajar con diccionarios.
         */
        else if( string( argv[1] ) == "descomprimir" )
        {
            string _direccion_descomprimido;

            Entrada_Salida.Sacar_raiz( argv, _direccion_descomprimido );
    
            ofstream descomprimido; 
            descomprimido.open( _direccion_descomprimido.c_str() );
            if( !descomprimido ) {cout<<endl<<"No se abrió el archivo descomprimido.txt :("; return;}
            else cout << "--> Archivo creado: " << _direccion_descomprimido << endl;

            ifstream in_tab_descomprimir;
            in_tab_descomprimir.open( argv[2] );
            if( !in_tab_descomprimir ) {cout<<endl<<"No se abrió el archivo in_tab_descomprimir :("; return;}

            ifstream in_rep_descomprimir;
            in_rep_descomprimir.open( argv[3] );
            if( !in_rep_descomprimir ) {cout<<endl<<"No se abrió el archivo in_rep_descomprimir :("; return;}


            map <string, string> _palabras = {}; 

            while( !in_tab_descomprimir.eof() )
            {
                _indice_base = 0;
                _cadena_base.clear();
                _palabra_base.clear();
                getline( in_tab_descomprimir, _cadena_base );

                Entrada_Salida.Sacar_Palabra ( _cadena_base, _indice_base, _palabra_base ); 
                Entrada_Salida.Sacar_Palabra ( _cadena_base, _indice_base, _palabra_comparar );

                _palabras.insert({ _palabra_comparar, _palabra_base });
            } 

            
            in_tab_descomprimir.clear();
            in_tab_descomprimir.seekg( in_tab_descomprimir.beg );
            _palabra_repetida = false;
            while( !in_rep_descomprimir.eof() )
            {
                _indice_base = 0;
                _cadena_base.clear();
                _palabra_base.clear();
                getline( in_rep_descomprimir, _cadena_base );

                for(int i = 0; i<Entrada_Salida.Contar_Palabras( _cadena_base ); i++)
                {
                    Entrada_Salida.Sacar_Palabra ( _cadena_base, _indice_base, _palabra_base );
                    _palabra_repetida = false;
                        
                    for (auto itr = _palabras.begin(); itr != _palabras.end(); ++itr) 
                    { 
                        if( _palabra_base == itr->first )
                        {
                            _palabra_repetida = true;
                            _palabra_comparar = itr->second;
                            break;
                        }
                    } 

                    if( _palabra_repetida )
                    {
                        descomprimido << _palabra_comparar << " ";
                    }
                    else
                    {
                        descomprimido << _palabra_base << " ";
                    }

                } 

                descomprimido << "\n";

            } 


            in_tab_descomprimir.close();
            in_rep_descomprimir.close();
            descomprimido.close(); 

            in_tab_descomprimir.clear();
            in_rep_descomprimir.clear();
            descomprimido.clear(); 
        }
        else
        {
            _retorno = FALLO;
        }

    } 

    this->imprimir_error( _retorno );    
};


Compresor::~Compresor(){};
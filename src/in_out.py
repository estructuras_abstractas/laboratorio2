#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" 
@author Jorge Muñoz Taylor
@date 21/01/2020

@file in_out.py
@brief La clase Entrada_Salida_Texto maneja el conteo de palabras, la extracción del nombre del archivo
y entrada/salida de parámetros.
"""

import sys
import os
import re
from pathlib import Path

## @class Entrada_Salida_Texto Contiene los metodos que permiten analizar un archivo de texto y generar las salidas correspondientes.
class Entrada_Salida_Texto( object ):

    ## @brief Constructor de la clase.
    def __init__(self):
        return

    ## @brief Se verifica si los argumentos ingresados por el usuario son correctos
    def Verifica_Parametros_Entrada(self):
        #if len( sys.argv ) == 4:

        if sys.argv[1] == "comprimir":

            if len( sys.argv ) == 4:    
                if (sys.argv[2]).isdigit():
                    if os.path.isfile( sys.argv[3] ): #Si el archivo existe
                        return True

                    else:
                        print ("\n")
                        print ("-->El archivo no existe")
                        print ("\n")
                        return False
                else:
                    print ("\n")
                    print ("-->El argumento 2 debe ser un numero")  
                    print ("\n")
                    return False  
            else:  
                print ("\n")
                print ("-->Tiene que ingresar 3 argumentos")
                print ("\n")
                return False


        elif sys.argv[1] == "descomprimir":
            
            if len( sys.argv ) == 4:    

                if (sys.argv[2]).isdigit() and (sys.argv[3]).isdigit():

                    print ("\n")
                    print ("--> Los argumentos 2 y 3 no pueden ser un números :/")  
                    print ("\n")
                    return False 

                else:
                    #Verifica si los archivos existen
                    if os.path.isfile( sys.argv[2] ) and os.path.isfile( sys.argv[3] ):
                        return True

                    else:
                        print ("\n")
                        print ("-->El archivo no existe")
                        print ("\n")
                        return False

            else:  
                print ("\n")
                print ("-->Tiene que ingresar 3 argumentos :)")
                print ("\n")
                return False

        else:
            print ("\n")
            print ("Para comprimir: comprimir\n")
            print ("Para descomprimir: descomprimir\n")
            return False


    ## @brief Obtiene la raiz del archivo ingresado
    ## @param direccion Dirección real del archivo.
    def Sacar_Raiz_Direccion( self, direccion ):

        ## Extrae el nombre del archivo sin la entensión
        _raiz = Path( direccion ).stem

        #_tab = _raiz + ".tab"
        #_rep = _raiz + ".rep"

        return _raiz #_tab, _rep


    ## @brief Saca las palabras de una oracion
    ## @param _cadena Cadena de palabras que serán extraídas.
    def Sacar_Palabras( self, _cadena ):
            
        palabras = ""
        
        palabras = _cadena.split()##< Saca todas las palabras con signos de puntuación

        return palabras


    ## @brief Cuenta las palabras de una oracion
    ## @param _cadena Cadena de palabras que serán contadas.
    def Contar_Palabras( self, _cadena ):
        return len( _cadena.split() )             
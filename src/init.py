#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" 
@author Jorge Muñoz Taylor
@date 21/01/2020

@file init.py
@brief
"""

import sys
from in_out import Entrada_Salida_Texto

## Definicion de la funcion main
def main():
    ## Instanciacion de objeto tipo Entrada_Salida_Texto
    In_Out = Entrada_Salida_Texto()

    if In_Out.Verifica_Parametros_Entrada():

        ## Comprime el archivo
        if sys.argv[1] == "comprimir":

            #Lista que contiene las palabras leidas 
            _codigo = 0
            _lista_palabras = set()
            _lista_validos = set()

            _tab = In_Out.Sacar_Raiz_Direccion( sys.argv[3] ) + ".tab"
            _rep = In_Out.Sacar_Raiz_Direccion( sys.argv[3] ) + ".rep"

            _texto = open( sys.argv[3], "r")    
            _archivo_tab = open( _tab, "w+" )
            _archivo_rep = open( _rep, "w+" )

        
            lineas_del_texto = _texto.readlines() ## Saca todas las líneas del archivo txt

            ## Aquí se crea el archivo .tab donde se guardan las palabras cuyo orden de 
            ##repetición es igual o mayor al umbral especificado por el usuario.
            for linea in lineas_del_texto:
            
                _palabras = In_Out.Sacar_Palabras ( linea ) ##Saca todas las palabras de la línea

                for i in _palabras: ##Recorre todas las palabras de la línea

                    _contador_palabras = 0

                    if not i in _lista_palabras: ##Determina si la palabra está en la lista

                        _lista_palabras.add( i )
        
                        for linea_comparar in lineas_del_texto:

                            _palabras_linea_comparar = In_Out.Sacar_Palabras( linea_comparar )

                            for j in _palabras_linea_comparar:
                            
                                if j == i:
                                    _contador_palabras += 1
                
                        if _contador_palabras >= int(sys.argv[2]):
                            _archivo_tab.write( "%s $%d\n" %(i, _codigo) )
                            _lista_validos.add( i )
                            _codigo+=1     

                _archivo_rep.write( "\n" )
            

            _archivo_rep.close()
            _archivo_tab.close()
            _archivo_rep = open( _rep, "w+" )

            abrir_tab = open( _tab, "r+" )
            leer_tab = abrir_tab.readlines()
        

            ## Recorre todas las lineas del texto
            ## Aquí se crea el archivo .rep donde se construye el texto codificado.
            for linea in lineas_del_texto:

                _palabras = In_Out.Sacar_Palabras ( linea ) ##Saca todas las palabras de la línea
            
                ## Recorre toda la linea
                for i in _palabras: ## Recorre todas las palabras

                    if i in _lista_validos:
                        
                        for linea_tab in leer_tab:   

                            palabra_codigo_tab = In_Out.Sacar_Palabras ( linea_tab )
                                            
                            if i == palabra_codigo_tab[0]:

                                _archivo_rep.write( palabra_codigo_tab[1] + " " )
                                break
                                
                    else:
                        _archivo_rep.write( "%s " %i )
                            
                _archivo_rep.write( "\n" )
            

            _texto.close()   
            _archivo_tab.close()
            _archivo_rep.close()
            abrir_tab.close()




        ## Descomprime el archivo
        elif sys.argv[1] == "descomprimir":

            archivo_destino = In_Out.Sacar_Raiz_Direccion( sys.argv[3] ) + "_descomprimido.txt"

            _archivo_tab = open( sys.argv[2], "r" )
            _archivo_rep = open( sys.argv[3], "r" )
            _archivo_des = open( archivo_destino, "w+" )

            lineas_rep = _archivo_rep.readlines()
            lineas_tab = _archivo_tab.readlines()

            for linea in lineas_rep:
                
                palabra_rep = In_Out.Sacar_Palabras( linea )
                
                for i in palabra_rep:

                    for linea_tab in lineas_tab:

                        estaba_la_palabra = False
                        
                        palabra_codigo_tab = In_Out.Sacar_Palabras( linea_tab )

                        if i == palabra_codigo_tab[1]:

                            _archivo_des.write( palabra_codigo_tab[0] + " " )
                            estaba_la_palabra = True
                            break


                    if estaba_la_palabra == False:
                        _archivo_des.write( i + " " )


                _archivo_des.write( "\n" )

            
        else:
            print ("\n")
            print ("ERROR\n")
           
    else:
        print ("--> Error con los parametros")
        print ("\n")
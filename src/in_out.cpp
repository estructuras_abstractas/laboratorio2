#include "../include/in_out.h"

entrada_salida_texto::entrada_salida_texto()
{
    _cont_codigo = 0;//< Lleva el conteo de las palabras que tienen asignado un codigo.
}

entrada_salida_texto::~entrada_salida_texto(){}


int entrada_salida_texto::Verifica_Parametros_Entrada( int argc, char** argv)
{ 
    /**
     * Verifica el número de argumentos.
     */
    if( argc == 4 )
    {
        if( string( argv[1] ) == "comprimir" )
        {
            if( es_un_numero( argv[2]) )
            {           
                ifstream archivo( argv[3] );

                if( archivo.fail() ) 
                    return ARCHIVO_NO_EXISTE;
                
                else 
                    return EXITO;
                
                archivo.close();          
            }
            else 
                return FALLO; 
        }

        /**
         * Verifica si se descomprime el archivo.
         */
        else if( string( argv[1] ) == "descomprimir" )
        {         
            ifstream archivo2( argv[2] );  
            if( archivo2.fail() )
                return ARCHIVO_NO_EXISTE;

            if( string(argv[2]).substr( string(argv[2]).find_last_of(".") + 1) != "tab") 
                return ARCHIVO_NO_EXISTE;

            archivo2.close();    

            ifstream archivo( argv[3] );   
            if( archivo2.fail() )
                return ARCHIVO_NO_EXISTE;

            /**
             * Determina si la extension del archivo es .rep.
             */
            if( string(argv[3]).substr( string(argv[3]).find_last_of(".") + 1) != "rep") 
                return ARCHIVO_NO_EXISTE;
 
            archivo2.close(); 
            return EXITO;
        }
        else
            return FALLO;

    }
    else if( argc != 4)
        return FALLO;

    else
        return ERROR;
 
}


void entrada_salida_texto::Sacar_Raiz_Direccion( char** argv, string &rep, string &tab )
{
    int i = 0;//< Es un contador genérico.
    string direccion;//< Dirección completa del archivo.
    string direccion_verdadera;//< Guarda únicamente el nombre del archivo sin extensión.
    
    rep.clear();//< Se asegura de limpiar la variable.
    tab.clear();//< Se asegura de limpiar la variable.

    while ( argv[3][i] != '\0' )
    {
        direccion.push_back( argv[3][i] );
          
        i++;
    }

    for ( i=0; i<4; i++)
    {
        direccion.pop_back();
    }


    for ( i = direccion.length()-1; i>=0; i-- )
    {
        if( direccion.at( i ) != '/' )
            direccion_verdadera = direccion.at(i) + direccion_verdadera; 

        else 
            i = 0;
    }
        
    cout << '\n';

    rep = direccion_verdadera + ".rep";
    tab = direccion_verdadera + ".tab";
}


void entrada_salida_texto::Sacar_raiz( char** argv, string &str )
{
    int i = 0;//< Es un contador genérico.

    while ( argv[3][i] != '\0' )
    {
        str.push_back( argv[3][i] );     
    
        i++;
    }

    for ( i=0; i<4; i++)
    {
        str.pop_back();
    }

    str.append( string( "_descomprimido.txt" ) );
}


void entrada_salida_texto::Sacar_Palabra( string linea, int &indice, string &palabra)
{
    int _cont = 0;
    palabra.clear();

    for( int i = indice; i < linea.length(); i++ )
    {
        if( linea[i] != ' ')
        {
            _cont = 1;

            palabra.push_back( linea[i] );
        }    

        indice = i;
        if( linea[i] == ' ' && _cont == 1 )
        {
            i = linea.length();
        }
    } 

} 


int entrada_salida_texto::Contar_Palabras( string linea )
{
    int _cont = 0;
    
    linea.push_back(' ');

    for (int i = 0; i<linea.length(); i++)
    {
        if( linea[i] != ' ' )
        {           
            while( linea[i] != ' ' )
                i++;

            _cont++;
        }       
    }  

    return _cont; 
}


bool entrada_salida_texto::es_un_numero(char* _cadena_base)
{
    bool si_es_numero = true;
    while (*_cadena_base)
    {
        if (!isdigit(*_cadena_base++))
            si_es_numero = false;
    }
    return (si_es_numero)? true:false;
}


string entrada_salida_texto::Codigo()
{
    string _simbolo = "$";
    return _simbolo + to_string(_cont_codigo++);
}
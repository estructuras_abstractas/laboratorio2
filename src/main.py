#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" 
@date 21/01/2020

@file main.py
@brief Función principal que ejecuta el método main().
"""

from init import main
from time import time

## Se ejecuta la funcion main que se definio en el archivo init.py
if __name__ == "__main__":
    tiempo_inicio = time()

    main()
    
    tiempo_transcurrido = time() - tiempo_inicio
    
    print ("\n**********************************************\n")
    print ("Tiempo de ejecucion: %f segundos" %tiempo_transcurrido)
    print ("\n**********************************************\n")
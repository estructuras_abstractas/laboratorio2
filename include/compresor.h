#pragma once

/**
 * @date 21/01/2020
 * 
 * @file compresor.h
 * @brief Definición de la estructura de la clase compresor
 */

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <map>

#include "../include/in_out.h"

using namespace std;

/**
 * @class Compresor
 * @brief 
 */
class Compresor
{
    public:
        
        /** 
         * @brief Contiene la lógica del programa. 
         * @param argc Contiene el número de parámetros introducidos.
         * @param argv Contiene los parámetros introducidos.
        */
        Compresor(int argc, char** argv );


        ~Compresor();


        /** @brief Imprime en la terminal como se usar el programa. */
        void print_usage();
        

        /** @brief Si ocurrio un error inesperado lo indica en la terminal. */
        void print_error();
        

        /**
         * @brief Despliega en pantalla si ocurrio un error durante la ejecucion de la funcion.
         * En caso de que ocurra un fallo de introduccion de argumentos o un error
         * inesperado del codigo, imprime dicha informacion en la terminal.
         * 
         * @param imprimir_error Tipo de error regresado, esto permite elegir que se imprimirá en la terminal.
         */
        void imprimir_error( int retorno);
};
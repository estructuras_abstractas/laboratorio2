#pragma once

/**
 * @date 21/01/2020
 * 
 * @file in_out.h
 * @brief Definición de la estructura de la clase in_out.
 */

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

/**
 * @enum Valores_Retorno 
 * @brief Enum para definir si la funcion termina con exito, fallo u ocurre un error imprevisto.
 */
enum Valores_Retorno
{
    EXITO,
    FALLO,
    TERMINAR,
    ERROR,
    ARCHIVO_NO_EXISTE
};


/**
 * @class entrada_salida_texto 
 * @brief Se encarga de procesar las lineas de texto del archivo
 */
class entrada_salida_texto
{
    public:
        entrada_salida_texto();
        ~entrada_salida_texto();


        /**
         * @brief Verifica que la cantidad de parametros es correcta y si el primer argumento es un numero.
         * @param argc Cuantos argumentos escribió  el usuario.
         * @param argv Los argumentos que escribió el usuario.
         * @return Si todo salió bien regresa EXITO, en caso contrario regresa otros valores
         */
        int Verifica_Parametros_Entrada( int argc, char** argv );
        

        /**
         * @brief Extrae el nombre del archivo sin la extension .txt para luego utilizarla en la creacion de los archivos rep y tab.
         * @param argv Dirección del archivo que se desea comprimir.
         * @param rep Variable donde se guardará el nombre del archivo con extensión .rep
         * @param tab Variable donde se guardará el nombre del archivo con extensión .tab
         */
        void Sacar_Raiz_Direccion( char** argv, string &rep, string &tab );
        

        /**
         * @brief Encuentra una palabra en un _cadena_base de strings.
         * @param linea Una línea del archivo a comprimir.
         * @param indice Indica por cual posición de la línea está la función.
         * @param palabra Cada palabra identificada de la línea se almacena aquí.
         */
        void Sacar_Palabra( string linea, int &indice, string &palabra);
        

        /**
        * @brief Cuenta las palabras de la oracion.
        * @param linea Guarda una línea del archivo a comprimir.
        * @return El número de palabras de la línea.
        */        
        int Contar_Palabras( string linea );


        /**
        * @brief Extrae la raiz del archivo ingresado.
        * @param argv Dirección del archivo.
        * @param str Guarda el nombre del archivo con extensión _descomprimido.txt
        */
        void Sacar_raiz( char** argv, string &str );
        

        /**
         * @brief Identifica si la _cadena_base es un numero, regresa true de ser asi.
         * @param _cadena_base Línea del archivo que se quiere analizar.
         * @return True si no es un número.
         */
        bool es_un_numero( char* _cadena_base );


        /**
        * @brief Crea el codigo de las palabras repetidas.
        * @return El código correspondiente a la palabra.
        */
        string Codigo();

    private:
        
        int _cont_codigo; //< Lleva el conteo de las palabras que tienen asignado un codigo. 
};
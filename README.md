# Laboratorio 2: Compresión y descompresión en C++


## Integrantes
```
Jorge Munoz Taylor
Alexander Calderon Torres
Roberto Acevedo Mora
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio2
```

Por último ejecute el make:
```
>>make
```
Esto compilará el programa y el doxygen.

## En C++
### Ejecutar el programa
Para comprimir:
```
./bin/lab2 comprimir #UMBRAL PATH/archivo.txt
```

Para descomprimir:
```
./bin/lab2 descomprimir ./archivo.rep ./archivo.tab
```

## En Python
### Ejecutar el programa
Para comprimir:
```
python3 ./src/main.py comprimir #UMBRAL PATH/archivo.txt
```

Para descomprimir:
```
python3 ./src/main.py descomprimir ./archivo.rep ./archivo.tab
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio2
```

Para compilar el doxygen hay dos formas:
```
>>make #Compila el proyecto y el doxygen
```
```
>>make doxygen #Sólo compila el doxygen
```

Ambos generarán el archivo PDF con la documentación creada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de docs/latex
```

## Dependencias
Debe tener instalado CMAKE y MAKE en la máquina:
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```